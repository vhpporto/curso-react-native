import React, { Component } from 'react'
import { SafeAreaView,View, StyleSheet, Text, ImageBackground, FlatList, TouchableOpacity, Platform } from 'react-native'
import moment from 'moment'
import 'moment/locale/pt-br'
import todayImage from '../../assets/imgs/today.jpg'
import commomStyles from '../commomStyles'
import Task from '../components/Task'
import Icon from 'react-native-vector-icons/FontAwesome'


export default class Agenda extends Component {

    state = {
        tasks: [
            { id: Math.random(), desc: 'Comprar Curso',
                estimateAt: new Date(), doneAt: new Date() },
            { id: Math.random(), desc: 'FInalizar curso',
                estimateAt: new Date(), doneAt: null },
            { id: Math.random(), desc: 'FInalizar curso',
                estimateAt: new Date(), doneAt: null },
            { id: Math.random(), desc: 'FInalizar curso',
                estimateAt: new Date(), doneAt: null },
            { id: Math.random(), desc: 'FInalizar curso',
                estimateAt: new Date(), doneAt: null },
            { id: Math.random(), desc: 'FInalizar curso',
                estimateAt: new Date(), doneAt: null },
            { id: Math.random(), desc: 'FInalizar curso',
                estimateAt: new Date(), doneAt: null },
            { id: Math.random(), desc: 'FInalizar curso',
                estimateAt: new Date(), doneAt: null },
            { id: Math.random(), desc: 'FInalizar curso',
                estimateAt: new Date(), doneAt: null },
            { id: Math.random(), desc: 'FInalizar curso',
                estimateAt: new Date(), doneAt: null },
            { id: Math.random(), desc: 'FInalizar curso',
                estimateAt: new Date(), doneAt: null },
            { id: Math.random(), desc: 'FInalizar curso',
                estimateAt: new Date(), doneAt: null },
        ],
        visibleTasks: [],
        showDoneTasks: true
        
    }


    filterTasks = () => {
        let visibleTasks = null
        if (this.state.showDoneTasks) {
            visibleTasks = [...this.state.tasks]
        } else { 
            const pending = task => task.doneAt === null
            visibleTasks = this.state.tasks.filter(pending)
        }
        this.setState({ visibleTasks })
    }

    toggleFilter = () => {
        this.setState({ showDoneTasks: !this.state.showDoneTasks}, this.filterTasks)
    }

    // componentDidMount = () => {
    //     this.filterTasks()
    // }



    toggleTask = id => {
        let tasks = this.state.tasks.map(task => {
            if (task.id === id) {
                task = {...task}
                task.doneAt = task.doneAt ? null : new Date()
            }
            return task
        })
        this.setState({tasks})
    }



  render() {
    return (
            <SafeAreaView style={styles.container}>
                <ImageBackground style={styles.background}
                    source={todayImage}>
                    <View style={styles.iconBar}>
                        <TouchableOpacity onPress={this.toggleFilter}>
                            <Icon name={this.setState.showDoneTasks ? 'eye' : 'eye-slash'}
                            size={20} color={commomStyles.colors.secondary} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.titleBar}>
                        <Text style={styles.title}>Hoje</Text>
                        <Text style={styles.subTitle}>
                            {moment().locale('pt-br').format('ddd, D [de] MMMM')}
                        </Text>
                    </View>

                </ImageBackground>
                <View style={styles.taskContainer}>
                    <FlatList data={this.state.visibleTasks}
                        keyExtractor={item => `${item.id}`}
                        renderItem={({item}) => 
                            <Task {...item} toggleTask={this.toggleTask} /> } />
                        

                </View>
            </SafeAreaView>
    )
  }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    background: {
        flex: 3
    },
    titleBar: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    title: {
        fontFamily: commomStyles.fontFamily,
        color: commomStyles.colors.secondary,
        fontSize: 50,
        marginLeft: 20,
        marginBottom: 10,
    },
    subTitle: {
        fontFamily: commomStyles.fontFamily,
        color: commomStyles.colors.secondary,
        fontSize: 20,
        marginLeft: 20,
        marginBottom: 30,
    },
    taskContainer: {
        flex: 7
    },
    iconBar: {
        marginTop: Platform.OS === 'ios' ? 30 : 10,
        marginHorizontal: 20,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    }


})